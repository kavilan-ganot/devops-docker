FROM node:12-alpine

ENV PORT=80
ENV NODE_ENV=development

WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 80
CMD ["node","app.js"]